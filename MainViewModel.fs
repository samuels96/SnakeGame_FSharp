﻿namespace ViewModels

open System
open System.Timers

open ViewModule
open ViewModule.FSharp
open System.Collections.ObjectModel
open System.Windows.Data

open Helper
open SnakeOperations

type MainViewModel() as self = 
    inherit ViewModelBase()

    let mutable courseHeight = 512
    let mutable courseWidth = 512
    let courseDimensionPropertyChanged =
        self.RaisePropertyChanged("CourseHeight")
        self.RaisePropertyChanged("CourseWidth")

    let courseFieldResolution = 16
    let courseVerticalOffset = 32
    let courseXLowBoundary = 0
    let courseXHighBoundary = courseWidth - (courseFieldResolution*2)
    let courseYLowBoundary = 0
    let courseYHighBoundary = courseHeight  - (courseFieldResolution*2) - (courseVerticalOffset*2)

    let isSnakeGoingOutOfBoundary(snakeHead : ObservableObject) =
        (snakeHead.X < courseXLowBoundary) || (snakeHead.X > courseXHighBoundary) ||
        (snakeHead.Y < courseYLowBoundary) || (snakeHead.Y > courseYHighBoundary) 

    let snakeEntitiesLock = new Object()
    let foodEntitiesLock = new Object()
    let compCollLock = new Object()


    let initialSnake = 
        let shead = SnakeHead()   :> SnakePartObj
        let sbody1 = SnakeBody()  :> SnakePartObj
        let sbody2 = SnakeBody()  :> SnakePartObj
        let sbody3 = SnakeBody()  :> SnakePartObj
        let sbody4 = SnakeBody()  :> SnakePartObj
        let sbody5 = SnakeBody()  :> SnakePartObj
        shead.SetX(96)
        shead.SetY(32)
        sbody1.SetX(80)
        sbody1.SetY(32)
        sbody2.SetX(64)
        sbody2.SetY(32)
        sbody3.SetX(48)
        sbody3.SetY(32)
        sbody4.SetX(32)
        sbody4.SetY(32)
        [|shead; sbody1; sbody2; sbody3; sbody4|]


    let snakeEntities : ObservableCollection<ObservableObject> = 
        let entCollection = new ObservableCollection<ObservableObject>()
        for part in initialSnake do
            entCollection.Add(part)

        BindingOperations.EnableCollectionSynchronization(entCollection, snakeEntitiesLock);
        entCollection

    let snakeEntitiesReset() = 
        let l = lock snakeEntitiesLock
        l(fun (_) ->
            snakeEntities.Clear()
            for part in initialSnake do
                snakeEntities.Add(part)
        )

    let foodEntities : ObservableCollection<ObservableObject> =
        let entCollection = new ObservableCollection<ObservableObject>()
        let food = Food()
        food.SetX(128)
        food.SetY(64)
        entCollection.Add(food)
        BindingOperations.EnableCollectionSynchronization(entCollection, foodEntitiesLock);
        entCollection

    let isInCollisionWithObjFromCollection(obj : ObservableObject, coll : ObservableCollection<ObservableObject>) =
        let mutable result : Option<int> = None
        let mutable i = 0
        while i < coll.Count && result.IsNone do
            let item = coll.Item(i)
            if (item.X = obj.X && item.Y = obj.Y) then
                result <- Some(i)
            i <- i + 1
        result

    let compositeEntities = 
        let compColl = new CompositeCollection()

        let snakeCollectionContainer = new CollectionContainer()
        snakeCollectionContainer.Collection <- snakeEntities

        let foodCollectionContainer = new CollectionContainer()
        foodCollectionContainer.Collection <- foodEntities

        compColl.Add(snakeCollectionContainer) |> ignore
        compColl.Add(foodCollectionContainer) |> ignore
        BindingOperations.EnableCollectionSynchronization(compColl, compCollLock);
        compColl

    let mutable gameFreeze = true
    let mutable currDirection = RIGHT

    let nextMoveAvailableInterval = 60.
    let mutable nextMoveAvailable = true
    let nextMoveTimer = 
        let timer = new Timer(nextMoveAvailableInterval)
        timer.Elapsed.Add(fun (_) -> 
            nextMoveAvailable <- true)
        timer.AutoReset <- false
        timer

    let createFoodAtRandomPos() =
        let rnd = Random()
        let tempRndX = rnd.Next() % 300
        let tempRndY = rnd.Next() % 300

        let x = tempRndX - (tempRndX % courseFieldResolution)
        let y = tempRndY - (tempRndY % courseFieldResolution)

        let food = Food()
        food.SetX(x)
        food.SetY(y)
        food

    let foodSpawnInterval = 500.
    let foodSpawnTimer =
        let timer = new Timer(foodSpawnInterval)
        timer.Elapsed.Add(fun (_) ->
            let mutable food = createFoodAtRandomPos()
            while (isInCollisionWithObjFromCollection(food, snakeEntities)).IsSome do
                food <- createFoodAtRandomPos()

            let l = lock foodEntitiesLock
            l(fun (_) ->
                foodEntities.Add(food)
            )
        )
        timer.AutoReset <- false
        timer

    let snakeGame = SnakeGame()

    let snakeMoveValidation1 input = 
        let newHead : ObservableObject = fst input
        let oldHeadReplacement : ObservableObject = snd input

        let maybeSnakeCollidesWithItself = isInCollisionWithObjFromCollection(newHead, snakeEntities)
        match maybeSnakeCollidesWithItself with
        | Some(_) ->
            snakeGame.DecreaseLives()
            self.RaisePropertyChanged("Lives")
            snakeEntitiesReset()
            if snakeGame.Lives.IsSome then
                snakeGame.State <- CRASH
            else snakeGame.State <- GAMEOVER
            SnakeCollision input 
        | None ->
            NoCollision input 

    let snakeMoveValidation2 input = 
        let newHead : ObservableObject = fst input
        let oldHeadReplacement : ObservableObject = snd input

        let maybeSnakeEatsFood = isInCollisionWithObjFromCollection(newHead, foodEntities)
        match maybeSnakeEatsFood with
        | Some(fruitIdx) ->
            snakeGame.IncreaseScore(10ul)
            self.RaisePropertyChanged("Score")
            //Remove food from collection
            let l = lock foodEntitiesLock
            l(fun (_) ->
                foodEntities.RemoveAt(fruitIdx)
            )
            foodSpawnTimer.Start()
            FoodCollision input
        | None ->
            //Cut tail
            snakeEntities.RemoveAt(snakeEntities.Count - 1)
            NoCollision input

    let snakeMoveValidation3 input = 
        let newHead : ObservableObject = fst input
        let oldHeadReplacement : ObservableObject = snd input

        let l = lock snakeEntitiesLock
        l(fun (_) ->
            snakeEntities.Insert(0, newHead)
            snakeEntities.Item(1) <- oldHeadReplacement        
            )
        NoCollision input

    let snakeMove(dir : Direction) =
        let snakeLen = snakeEntities.Count - 1
        let oldHead = snakeEntities.Item(0)

        let newHead = createHead(dir)
        newHead.SetX(oldHead.X)
        newHead.SetY(oldHead.Y)

        let oldHeadReplacement = SnakeBody() :> ObservableObject
        oldHeadReplacement.SetX(oldHead.X)
        oldHeadReplacement.SetY(oldHead.Y)

        match dir with
        | UP    -> newHead.SetY(oldHead.Y-courseFieldResolution)
        | DOWN  -> newHead.SetY(oldHead.Y+courseFieldResolution)
        | LEFT  -> newHead.SetX(oldHead.X-courseFieldResolution)
        | RIGHT -> newHead.SetX(oldHead.X+courseFieldResolution)
        | NONE -> ()

        if isSnakeGoingOutOfBoundary(newHead) then
            match dir with
            | UP    -> newHead.SetY(courseYHighBoundary)
            | DOWN  -> newHead.SetY(courseYLowBoundary)
            | LEFT  -> newHead.SetX(courseXHighBoundary)
            | RIGHT -> newHead.SetX(courseXLowBoundary)
            | NONE -> ()

        let snakeMoveValidationCombined =
            snakeMoveValidation1 >=> snakeMoveValidation2 >=> snakeMoveValidation3 

        let input = (newHead, oldHeadReplacement)
        snakeMoveValidationCombined input |> ignore

    let gameLoopTimer = 
        let timer = new Timer(100.)
        timer.Elapsed.Add(fun (_) ->
            match snakeGame.State with
            | NORMAL ->
                if snakeGame.Lives.IsSome then
                    snakeMove(currDirection)
            | CRASH ->
                gameFreeze <- true
                snakeGame.State <- NORMAL
                timer.Stop()
            | GAMEOVER ->
                gameFreeze <- true
                timer.Stop()
            )
        timer.AutoReset <- true
        timer

    let gameLoop() = 
        if snakeGame.HasLivesLeft() then
            if gameFreeze then
                gameLoopTimer.Start()
                gameFreeze <- false

    let keyUpProcedure(dir : Direction) = 
        //Key press starts the game loop
        gameLoop()
        if nextMoveAvailable then
            if not (isCounterDirection(currDirection, dir)) then
                nextMoveAvailable <- false
                currDirection <- dir
                nextMoveTimer.Start()

    let keyUpCommand = 
        self.Factory.CommandSync( fun () -> 
                    keyUpProcedure(UP)
                )
    let keyRightCommand = 
        self.Factory.CommandSync( fun () -> 
                    keyUpProcedure(RIGHT)
                )
    let keyDownCommand = 
        self.Factory.CommandSync( fun () -> 
                    keyUpProcedure(DOWN)
                )
    let keyLeftCommand = 
        self.Factory.CommandSync( fun () -> 
                    keyUpProcedure(LEFT)
                )
    let keyEnterCommand = 
        self.Factory.CommandSync( fun () -> 
                    if not (snakeGame.HasLivesLeft()) then
                        snakeGame.Reset()
                        self.RaisePropertyChanged("Lives")
                        self.RaisePropertyChanged("Score")
                )
    member this.Score with get() = snakeGame.Score
    member this.Lives with get() = if snakeGame.Lives.IsSome then snakeGame.Lives.Value else 0

    member this.CourseVerticalOffset with get() = courseVerticalOffset
    member this.CourseHeight 
        with get() = courseHeight
        and set(newVal) = courseHeight <- newVal
    member this.CourseWidth 
        with get() = courseWidth
        and set(newVal) = courseWidth <- newVal

    member this.Entities with get() : CompositeCollection = compositeEntities

    member this.KeyRightCommand = keyRightCommand
    member this.KeyDownCommand = keyDownCommand
    member this.KeyLeftCommand = keyLeftCommand
    member this.KeyUpCommand = keyUpCommand
    member this.KeyEnterCommand = keyEnterCommand

